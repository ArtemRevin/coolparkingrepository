﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.Text;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        private DateTime TimeOfTransaction { get; set; }
    
        private string IdOfVehicle { get; set; }

        public decimal Sum { get; set; }

        public TransactionInfo(string id, decimal sum)
        {
            TimeOfTransaction = DateTime.Now;
            IdOfVehicle = id;
            Sum = sum;
        }

        public string GetTransactionAsString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(TimeOfTransaction + " " + IdOfVehicle + " " + Sum);
            return sb.ToString();
        }
    }
}

