﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.IO;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal ParkingInitialBalance = 0;
        public static int ParkingCapacity = 10;
        public static int ParkingTaxTime = 5000;
        public static int ParkingLogTime = 60000;
        public static decimal CarTax = 2m;
        public static decimal TruckTax = 5m;
        public static decimal MotoTax = 1m;
        public static decimal BusTax = 3.5m;
        public static decimal Fine = 2.5m;
        public static string LogFile = new FileInfo(@"Logs/Log.txt").FullName;
        /*@"Logs/Log.txt";*/
    }
}
