﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
    
        public VehicleType VehicleType { get; }
        
        public decimal Balance { get; set; }
        
        public bool IsGeneratedId { get; }
    
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex pattern = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
    
            if (pattern.IsMatch(id))
            {
                Id = id;
            }
            else
            {
                Id = GenerateRandomRegistrationPlateNumber();
                IsGeneratedId = true;
                
                throw new ArgumentException();
            }

            if (balance < 0)
            {
                throw new ArgumentException();
            }
            else
            {
                Balance = balance;
            }
            
            VehicleType = vehicleType;
        }
    
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
    
            string generatedNumber = "";
    
            do
            {
                    
                if (generatedNumber.Length < 2 || generatedNumber.Length > 7)
                {
                    generatedNumber += (char)random.Next(65, 90);
                }
    
                if (generatedNumber.Length > 2 && generatedNumber.Length < 7)
                {
                    generatedNumber += random.Next(0, 10);
                }
    
                if (generatedNumber.Length == 2 || generatedNumber.Length == 7)
                {
                    generatedNumber += "-";
                }
                    
            } while (generatedNumber.Length < 10);
    
            return generatedNumber; 
        }

        public void VehicleToString()
        {
            Console.WriteLine($"ID: {Id} - Type: {VehicleType} - Balance: {Balance}");
        }
    }
}
