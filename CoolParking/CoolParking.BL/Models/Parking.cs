﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Timers;
using CoolParking.BL.Services;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        public decimal ParkingBalance { get; set; }

        public Dictionary<string, Vehicle> _listOfVehicles;
    
        private static Parking _parking;
        
        // try
        public List<TransactionInfo> LastTransactions = new ();
        public LogService LogService = new LogService();

        private Parking()
        {
            ParkingBalance = Settings.ParkingInitialBalance;
            _listOfVehicles = new Dictionary<string, Vehicle>(Settings.ParkingCapacity);
        }

        public static Parking ParkingInstance
        {
            get
            {
                if (_parking == null)
                {
                    _parking = new Parking();
                }

                return _parking;
            }
        }

        public bool IsExist(Vehicle vehicle)
        {
            foreach (var record in _listOfVehicles)
            {
                if (record.Key == vehicle.Id)
                {
                    return true;
                }
            }

            return false;
        }
        
        public bool IsExist(string vehicleId)
        {
            foreach (var record in _listOfVehicles)
            {
                if (record.Key == vehicleId)
                {
                    return true;
                }
            }

            return false;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            List<Vehicle> vehicles = new List<Vehicle>();

            foreach (var record in _listOfVehicles)
            {
                vehicles.Add(record.Value);
            }

            return new ReadOnlyCollection<Vehicle>(vehicles);
        }

        public Vehicle GetVehicle(string id)
        {
            return _listOfVehicles[id];
        }
        
        /*public void Write()
        {
            StringBuilder sb = new StringBuilder();
            // try
            foreach (var t in Parking.ParkingInstance.LastTransactions)
            {
                sb.AppendLine(t.GetTransactionAsString());
            }
            
            LogService.Write(sb.ToString());
            // try
            Parking.ParkingInstance.LastTransactions.Clear();
        }*/
        
        public void Withdraw()
        {
            foreach (var item in Parking.ParkingInstance._listOfVehicles)
            {
                Vehicle vehicle = item.Value;
                VehicleType vehicleType = vehicle.VehicleType;
                decimal tax = 0;
                decimal difference;

                switch (vehicleType)
                {
                    case VehicleType.PassengerCar :
                        tax = Settings.CarTax;
                        break;
                   
                    case VehicleType.Bus :
                        tax = Settings.BusTax;
                        break;
                   
                    case VehicleType.Motorcycle :
                        tax = Settings.MotoTax;
                        break;
                   
                    case VehicleType.Truck :
                        tax = Settings.TruckTax;
                        break;
                }

                if (vehicle.Balance >= tax)
                {
                    vehicle.Balance -= tax;
                    Parking.ParkingInstance.ParkingBalance += tax;
                }
                else if (vehicle.Balance < tax && vehicle.Balance > 0)
                {
                    difference = tax - vehicle.Balance;
                    decimal withdraw = vehicle.Balance + difference * Settings.Fine;
                    vehicle.Balance -= withdraw;
                    Parking.ParkingInstance.ParkingBalance += withdraw;
                }
                else
                {
                    decimal withdraw = tax * Settings.Fine;
                    vehicle.Balance -= withdraw;
                    Parking.ParkingInstance.ParkingBalance += withdraw;
                }

                TransactionInfo tInfo = new TransactionInfo(vehicle.Id, tax);
                // try
                Parking.ParkingInstance.LastTransactions.Add(tInfo);
            }
        }
    }
}

