﻿using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL;

public class Program
{
    public static void Main(string[] args)
    {
        TimerService tsWithdraw = new TimerService(Settings.ParkingTaxTime);
        TimerService tsLog = new TimerService(Settings.ParkingLogTime);
        LogService logService = new LogService();
        
        var parkServ = new ParkingService(tsWithdraw, tsLog, logService);

        while (true)
        {
            Console.WriteLine(new string('-', 30));
            Console.WriteLine("Welcome to the Parking Service");
            Console.WriteLine(new string('-', 30));
            Console.WriteLine("[1] - Show current balance of the parking");
            Console.WriteLine("[2] - Show sum of the profit from the last period");
            Console.WriteLine("[3] - Info about free places at the parking");
            Console.WriteLine("[4] - Show all transaction from the last period");
            Console.WriteLine("[5] - Show full history of the transactions");
            Console.WriteLine("[6] - Show all vehicles on the parking");
            Console.WriteLine("[7] - Add vehicle on the parking");
            Console.WriteLine("[8] - Remove vehicle from the parking");
            Console.WriteLine("[9] - Increase balance of the vehicle");
            Console.WriteLine("[q] - Exit");

            var key = Console.ReadKey();

            switch (key.KeyChar)
            {
                case '1':
                    Console.WriteLine($"\nBalance: {parkServ.GetBalance()}");
                    continue;
            
                case '2':
                    Console.WriteLine($"\nProfit fot the last period: {parkServ.GetBalanceForLastPeriod()}");
                    continue;
            
                case '3':
                    Console.WriteLine($"\nAmount of the free places at the parking: " +
                                      $"{parkServ.GetFreePlaces()} / {parkServ.GetCapacity()}");
                    continue;

                case '4':
                {
                    Console.WriteLine("\nTransaction from the last period:");
                    // try
                    foreach (var t in parkServ.GetLastParkingTransactions())
                    {
                        Console.WriteLine(t.GetTransactionAsString());
                    }
                    continue;
                }

                case '5':
                {
                    Console.WriteLine("\nFull history of the transactions:");
                    parkServ._logService.Read();
                    continue;   
                }

                case '6':
                {
                    Console.WriteLine("\nVehicles at the parking:");
                    foreach (var v in parkServ.GetVehicles())
                    {
                        v.VehicleToString();
                    }
                    continue;   
                }

                case '7':
                {
                    Console.WriteLine("\nDefine ID of the vehicle in correct format:");
                    var idOfVehicle = Console.ReadLine();
                    VehicleType vehicleType;
                    decimal convertedBalance;

                    while (true)
                    {
                        Console.WriteLine("Define type of the vehicle:");
                        Console.WriteLine("[1] - Passenger Car");
                        Console.WriteLine("[2] - Truck");
                        Console.WriteLine("[3] - Bus");
                        Console.WriteLine("[4] - Moto (Bike)");
                        var typeOfVehicle = Console.ReadLine();

                        switch (typeOfVehicle)
                        {
                            case "1" :
                                vehicleType = VehicleType.PassengerCar;
                                break;
                
                            case "2" :
                                vehicleType = VehicleType.Truck;
                                break;
                
                            case "3" :
                                vehicleType = VehicleType.Bus;
                                break;
                
                            case "4" :
                                vehicleType = VehicleType.Motorcycle;
                                break;
                
                            default :
                                Console.WriteLine("Incorrect type. Please, try again.");
                                continue;
                        }
                        
                        break;
                    }

                    while (true)
                    {
                        Console.WriteLine("Define initial balance of the vehicle:");
                        var balanceOfVehicle = Console.ReadLine();

                        try
                        {
                            convertedBalance = Convert.ToDecimal(balanceOfVehicle);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Incorrect sum. Please try again.");
                            continue;
                        }
                        
                        break;
                    }
                    
                    parkServ.AddVehicle(new Vehicle(idOfVehicle, vehicleType, convertedBalance));

                    continue;   
                }

                case '8':
                {
                    Console.WriteLine("\nDefine ID of the vehicle in correct format:");
                    var idOfVehicle = Console.ReadLine();
                    parkServ.RemoveVehicle(idOfVehicle);
                    continue;
                }

                case '9':
                {
                    Console.WriteLine("\nDefine ID of the vehicle in correct format:");
                    var idOfVehicle = Console.ReadLine();

                    decimal convertedSum;
                    
                    while (true)
                    {
                        Console.WriteLine("Define sum which want to add:");
                        var sum = Console.ReadLine();

                        try
                        {
                            convertedSum = Convert.ToDecimal(sum);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Incorrect sum. Please try again.");
                            continue;
                        }
                        
                        break;
                    }
                    
                    parkServ.TopUpVehicle(idOfVehicle, convertedSum);
                    
                    continue;   
                }
            }

            switch (key.KeyChar)
            {
                case 'q':
                    Console.WriteLine("\nGoodbye!");
                    break;
                
                default :
                    Console.WriteLine("\nAlternative quit");
                    break;
            }
            
            break;
        }
        
    }
}