﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public ITimerService _withdrawTimer;
        public ITimerService _logTimer;
        public ILogService _logService;

        /*public List<TransactionInfo> LastTransactions = new ();*/

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += WithdrawHandler;
            _logTimer.Elapsed += WriteHandler;

            _withdrawTimer.Start();
            _logTimer.Start();
        }
        
        public void Dispose()
        {
            _withdrawTimer.Stop();
            _logTimer.Stop();
            
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            
            Parking.ParkingInstance._listOfVehicles.Clear();
            Parking.ParkingInstance.ParkingBalance = 0;
        }

        public decimal GetBalanceForLastPeriod()
        {
            decimal sumForTheLastPeriod = 0;
            // try
            foreach (var t in Parking.ParkingInstance.LastTransactions)
            {
                sumForTheLastPeriod += t.Sum;
            }

            return sumForTheLastPeriod;
        }

        public decimal GetBalance()
        {
            return Parking.ParkingInstance.ParkingBalance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - Parking.ParkingInstance._listOfVehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.ParkingInstance.GetVehicles();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (!Parking.ParkingInstance.IsExist(vehicle))
            {
                if (GetFreePlaces() > 0)
                {
                    Parking.ParkingInstance._listOfVehicles.Add(vehicle.Id, vehicle);
                }
                else
                {
                    throw new InvalidOperationException();    
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!Parking.ParkingInstance.IsExist(vehicleId))
            {
                try
                {
                    throw new ArgumentException();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Vehicle with such ID is not found!");
                    throw;
                }
            }
            
            Vehicle vehicle = Parking.ParkingInstance._listOfVehicles[vehicleId];
            
            if (vehicle is {Balance: > 0})
            {
                Parking.ParkingInstance._listOfVehicles.Remove(vehicleId);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0 || !Parking.ParkingInstance.IsExist(vehicleId))
            {
                try
                {
                    throw new ArgumentException();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("Balance or vehicle ID is incorrect!");
                    throw;
                }
            }
            else
            {
                Vehicle vehicle = Parking.ParkingInstance.GetVehicle(vehicleId);
                vehicle.Balance += sum;
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            // try
            return Parking.ParkingInstance.LastTransactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void WriteHandler(object sender, ElapsedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            // try
            foreach (var t in Parking.ParkingInstance.LastTransactions)
            {
                sb.AppendLine(t.GetTransactionAsString());
            }
            
            _logService.Write(sb.ToString());
            // try
            Parking.ParkingInstance.LastTransactions.Clear();
        }

        public void WithdrawHandler(object sender, ElapsedEventArgs e)
        {
            /*Withdraw();*/
            Parking.ParkingInstance.Withdraw();
        }
        
        /*public void Withdraw()
        {
            foreach (var item in Parking.ParkingInstance._listOfVehicles)
            {
                Vehicle vehicle = item.Value;
                VehicleType vehicleType = vehicle.VehicleType;
                decimal tax = 0;
                decimal difference;

                switch (vehicleType)
                {
                   case VehicleType.PassengerCar :
                       tax = Settings.CarTax;
                       break;
                   
                   case VehicleType.Bus :
                       tax = Settings.BusTax;
                       break;
                   
                   case VehicleType.Motorcycle :
                       tax = Settings.MotoTax;
                       break;
                   
                   case VehicleType.Truck :
                       tax = Settings.TruckTax;
                       break;
                }

                if (vehicle.Balance >= tax)
                {
                    vehicle.Balance -= tax;
                    Parking.ParkingInstance.ParkingBalance += tax;
                }
                else if (vehicle.Balance < tax && vehicle.Balance > 0)
                {
                    difference = tax - vehicle.Balance;
                    decimal withdraw = vehicle.Balance + difference * Settings.Fine;
                    vehicle.Balance -= withdraw;
                    Parking.ParkingInstance.ParkingBalance += withdraw;
                }
                else
                {
                    decimal withdraw = tax * Settings.Fine;
                    vehicle.Balance -= withdraw;
                    Parking.ParkingInstance.ParkingBalance += withdraw;
                }

                TransactionInfo tInfo = new TransactionInfo(vehicle.Id, tax);
                // try
                Parking.ParkingInstance.LastTransactions.Add(tInfo);
            }
        }*/
    }
}