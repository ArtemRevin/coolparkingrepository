﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService():
            this(Settings.LogFile)
        {
            
        }
        
        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath, true))
            {
                sw.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                using (StreamReader sr = new StreamReader(LogPath))
                {
                    string logLine;
                    while ((logLine = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(logLine);
                    }
                }
                
                return sb.ToString();
            }
            catch (FileNotFoundException e)
            {
                throw new InvalidOperationException();
            }
        }
    }
}