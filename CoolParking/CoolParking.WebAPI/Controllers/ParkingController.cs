﻿using System.Collections;
using System.Collections.Generic;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ParkingController : ControllerBase
{
    //GET api/parking/balance
    [HttpGet("{balance}")]
    public ActionResult<decimal> Balance()
    {
        return Parking.ParkingInstance.ParkingBalance;
    }
        
    //GET api/parking/capacity
    [HttpGet("{capacity}")]
    public ActionResult<int> Capacity()
    {
        return Settings.ParkingCapacity;
    }
        
    //GET api/parking/freePlaces
    [HttpGet("{freeplaces}")]
    public ActionResult<int> Freeplaces()
    {
        return Settings.ParkingCapacity - Parking.ParkingInstance._listOfVehicles.Count;
    }
}