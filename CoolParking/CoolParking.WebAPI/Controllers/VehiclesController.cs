﻿using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers;

    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        Regex pattern = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
        
        //GET api/vehicles
        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> Vehicles()
        {
            return Parking.ParkingInstance.GetVehicles();
        }
        
        //GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Vehicles(string id)
        {
            if (!pattern.IsMatch(id))
            {
                return BadRequest();
            }
            
            Vehicle vehicle = Parking.ParkingInstance._listOfVehicles.Values.Single(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound();
            }

            return vehicle;
        }
        
        //POST api/vehicles
        [HttpPost]
        public ActionResult<Vehicle> Vehicles(string id, int vehicleType, decimal balance)
        {
            Vehicle vehicle;
            
            try
            {
                vehicle = new Vehicle(id, (VehicleType)vehicleType, balance);
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            JsonResult result = new JsonResult(vehicle);
            
            //return result;
            return Created(vehicle.Id, result);
        }
        
        //DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            if (!pattern.IsMatch(id))
            {
                return BadRequest();
            }
            
            Vehicle vehicle = Parking.ParkingInstance._listOfVehicles.Values.Single(v => v.Id == id);

            if (vehicle == null)
            {
                return NotFound();
            }

            return NoContent();
        }
        
        //GET api/transactions/last
        [HttpGet("api/transactions/last")]
        public ActionResult<TransactionInfo[]> TransactLast()
        {
            return Parking.ParkingInstance.LastTransactions.ToArray();
        }
        
        //GET api/transactions/all (только транзакции с лог файла)
        [HttpGet("api/transactions/all")]
        public ActionResult<string> TransactAll()
        {
            return Parking.ParkingInstance.LogService.Read();
        }
        
        //PUT api/transactions/topUpVehicle
        [HttpPut("topupvehicle/{id}/{sum}")]
        public ActionResult<Vehicle> TopUpVehicle(string id, decimal sum)
        {
            if (!pattern.IsMatch(id))
            {
                return BadRequest();
            }
                    
            Vehicle vehicle = Parking.ParkingInstance._listOfVehicles.Values.Single(v => v.Id == id);
        
            if (vehicle == null)
            {
                return NotFound();
            }
        
            JsonResult result = new JsonResult(vehicle);
            
            return result;
        }
    }